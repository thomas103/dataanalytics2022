library(data.table)
library(lubridate)
library(dplyr)
library(leaflet)
library(timeDate)
library(corrplot)
library(ggplot2)
library(plotly)

dtleads                <- fread("/srv/data/retailusecase/marketing_allleads.csv")

str(dtleads)

#1.) Major Landing Pages:
dtlp  <- dtleads[,list(Request=length(mql_id)
                       ,Closed=sum(Closed)
                       )
                 ,by=c("landing_page_id")
                 ]

#Faustformel: n*p*(1-p) > 7
dtlp$Threshold        <- 7/((dtlp$Closed/dtlp$Request)*(1-(dtlp$Closed/dtlp$Request)))
dtlp$LandingPage      <- ifelse(dtlp$Threshold< dtlp$Request,dtlp$landing_page_id,"others")
dtlp$LandingPage      <- as.factor(dtlp$LandingPage)
dtlp                  <- dtlp%>%select(landing_page_id,LandingPage)
dtleads               <- merge(dtleads,dtlp,by=c("landing_page_id"))

#Explore:
dtlpexp  <- dtleads[,list(Inquiries=length(mql_id)
                         ,ClosedDeals=sum(Closed)
                         ),by=c("LandingPage")]
dtlpexp$NotClosedDeals  <- dtlpexp$Inquiries-dtlpexp$ClosedDeals
dtlpexp$ConversionRate  <- dtlpexp$ClosedDeals/dtlpexp$Inquiries
dtlpexp$NotClosedDeal_s <- dtlpexp$NotClosedDeals/dtlpexp$Inquiries
dtlpexp$ClosedDeal_s    <- dtlpexp$ClosedDeals/dtlpexp$Inquiries

dtlpexp <- dtlpexp[order(-dtlpexp$ConversionRate),]

str(dtlpexp)

str(dtleads)

p <- plot_ly(dtlpexp, x = ~LandingPage, y = ~ClosedDeal_s, type = 'bar', name = 'ClosedDeals') %>%
  add_trace(y = ~NotClosedDeal_s, name = 'NotClosedDeals') %>%
  layout(yaxis = list(title = 'Inquiries'), barmode = 'stack')
p     
      

#2.) MArketing Channels:
dtleads$origin        <- as.factor(dtleads$origin)


#Explore:
dtorigexp  <- dtleads[,list(Inquiries=length(mql_id)
                         ,ClosedDeals=sum(Closed)
                         ),by=c("origin")]
dtorigexp$NotClosedDeals  <- dtorigexp$Inquiries-dtorigexp$ClosedDeals
dtorigexp$ConversionRate  <- dtorigexp$ClosedDeals/dtorigexp$Inquiries
dtorigexp$NotClosedDeal_s <- dtorigexp$NotClosedDeals/dtorigexp$Inquiries
dtorigexp$ClosedDeal_s    <- dtorigexp$ClosedDeals/dtorigexp$Inquiries

dtorigexp <- dtorigexp[order(-dtorigexp$ConversionRate),]
dtorigexp

#3) Time since first contact date:
dtleads$Closed_cat          <- as.factor(ifelse(dtleads$Closed==1,"Yes","No"))
maxdate                     <- max(dtleads$won_date)

dtleads$won_date             <- ifelse(dtleads$won_date=="",maxdate,dtleads$won_date)
dtleads$DurationSaleProcess  <- as.integer(difftime(as.timeDate(dtleads$won_date)
                                                    ,as.timeDate(dtleads$first_contact_date),units="days"))


sort(unique(dtleads$DurationSaleProcess))

 dftmp <- as.data.frame(dtleads)
    p     <- ggplot(dftmp, aes_string(x ="DurationSaleProcess")) +
      stat_density(aes(group = Closed_cat, color =Closed_cat ),position="identity",geom="line")

p

tmp   <-dtleads%>%select(Closed,origin,LandingPage,DurationSaleProcess) 
model <- glm(Closed~.,family="binomial",data=tmp)

summary(model)


model <- step(model)
summary(model)

test <- tmp[tmp$DurationSaleProcess==5,]
test  <- test[1,]
test
prob  <-predict(model,test,type="response")
prob




