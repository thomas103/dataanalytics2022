library(data.table)
library(lubridate)
library(dplyr)
library(leaflet)
library(arules)
library(arulesViz)
library(timeDate)
library(caret)
library(gbm)
library(corrplot)
library(pROC)
library(MLmetrics)
library(MLeval)

load("/srv/data/retailusecase/Orders_Reviews.rda",verbose = T)
dtorders                <- fread("/srv/data/retailusecase/Orders.csv",encoding = "Latin-1")
dtorders_items          <- fread("/srv/data/retailusecase/Orders_Items.csv",encoding = "Latin-1")
dtproductfeatures       <- fread("/srv/data/retailusecase/Masterdata_product.csv",encoding = "Latin-1")
dtcust                  <- fread("/srv/data/retailusecase/masterdata_customer.csv",encoding = "Latin-1")


#1.1) Fulfilment Performance:
dtorders$DeliveryDelay_days   <- as.integer(difftime(as.timeDate(dtorders$order_delivered_customer_date)
                                                ,as.timeDate(dtorders$order_estimated_delivery_date),units="days"))

dtorders                      <- dtorders[is.na(dtorders$DeliveryDelay)==F,]
dtorders$DeliveryDelay_cat    <- ifelse(dtorders$DeliveryDelay>0,"Yes","No")
dtorders$DeliveryDelay_cat    <- as.factor(dtorders$DeliveryDelay_cat)
dtorders$ApprovalTime_days    <- as.integer(difftime(as.timeDate(dtorders$order_approved_at)
                                                     ,as.timeDate(dtorders$order_purchase_timestamp),units="secs"))/(3600*24)

dtorders                      <- dtorders[is.na(dtorders$ApprovalTime_days)==F,]
dtorders$EstimatedDeliveryTime_days  <- as.integer(difftime(as.timeDate(dtorders$order_estimated_delivery_date)
                                                            ,as.timeDate(dtorders$order_purchase_timestamp),units="secs"))/(3600*24)


#1.2) Order Characteristics (Total Order Value, Freight Ratio)
dtorders_aov    <- dtorders_items[,list(price=sum(price)
                                        ,orders=length(order_item_id)
                                        ,freight_value=sum(freight_value)
),by=c("order_id")]  

dtorders_aov$AOV              <- dtorders_aov$price/dtorders_aov$orders
dtorders_aov$TotalOrderValue  <- dtorders_aov$price
dtorders_aov$FreighRatio      <- dtorders_aov$freight_value/(dtorders_aov$price+dtorders_aov$freight_value)

dtorders_aov[dtorders_aov$FreighRatio<=quantile(dtorders_aov$FreighRatio)[3] 
             ,"FreightRatio_cat"] <- "FR:Below Average"
dtorders_aov[dtorders_aov$FreighRatio>quantile(dtorders_aov$FreighRatio)[3] 
             & dtorders_aov$FreighRatio<=quantile(dtorders_aov$FreighRatio)[4]
             ,"FreightRatio_cat"] <- "FR:Above Average Moderate"
dtorders_aov[dtorders_aov$FreighRatio>quantile(dtorders_aov$FreighRatio)[4] 
             ,"FreightRatio_cat"] <- "FR:High Above Average"

dtorders                      <- merge(dtorders,dtorders_aov,by=c("order_id"))
dtorders$FreightRatio_cat     <- as.factor(dtorders$FreightRatio_cat)
dtorders$order_status         <- as.factor(dtorders$order_status)


#1.3) Customer Characteristics:
dtorders                 <- merge(dtorders,dtcust,by=c("customer_id"))
dtorders$customer_state  <- as.factor(dtorders$customer_state)


#1.4)Product Characteristics:
dtorders_prodchar                 <- merge(dtorders_items,dtproductfeatures,by=c("product_id"))
dtorders_prodchar$ProductVolume   <- dtorders_prodchar$product_length_cm*dtorders_prodchar$product_height_cm*dtorders_prodchar$product_width_cm
dtorders_prodchar                 <- dtorders_prodchar[,list(MaxProductWeight=max(product_weight_g)
                                              ,MaxProductVolume=max(ProductVolume)
                                              ,MinProductPhotosQty=min(product_photos_qty)
                                              ),by=c("order_id")]

dtorders_prodchar                 <- dtorders_prodchar[is.na(dtorders_prodchar$MaxProductVolume)==F 
                                                        & is.na(dtorders_prodchar$MinProductPhotosQty)==F
                                                        & is.na(dtorders_prodchar$MaxProductWeight)==F
                                                        ,]

dtorders                          <- merge(dtorders,dtorders_prodchar,by=c("order_id"))


#1.5) Final Selection:
dtreviews[dtreviews$review_score <3
          ,"review_score_bucket"]  <- "Poor"
dtreviews[dtreviews$review_score >=3
          ,"review_score_bucket"]  <- "Good"
dtreviews$review_score_bucket      <- as.factor(dtreviews$review_score_bucket)

dtorders           <- merge(dtorders,dtreviews,by=c("order_id"))
dtorders_final     <- dtorders%>%select(order_id,order_status,MaxProductVolume,MaxProductWeight,MinProductPhotosQty,FreighRatio,FreightRatio_cat
                                  ,customer_state,DeliveryDelay_days,DeliveryDelay_cat, ApprovalTime_days,EstimatedDeliveryTime_days
                                  ,orders,AOV,TotalOrderValue,review_score,review_score_bucket
                                  )
dtorders_final$review_score_bucket  <- relevel(dtorders_final$review_score_bucket, ref = "Poor")




#2.1) Correlation Matrix
numeric_features <- sapply(X = dtorders_final, FUN = is.numeric)
numeric_features <- names(dtorders_final)[numeric_features]



#Plot:
dforders_final  <- as.data.frame(dtorders_final)
corrplot(corr = cor(dforders_final[, numeric_features]))


dfcorr          <- cor(dforders_final[, numeric_features])
dfcorr          <- data.table(Features=rownames(dfcorr),ReviewScoreCorrelation=dfcorr[,"review_score"])
dfcorr

#3.2) Classification:
dtorders_final$order_id            <- NULL
dtorders_final$review_score        <- NULL

inTrain <- createDataPartition(y = dtorders_final$review_score_bucket, p = 0.7, list = FALSE)
df_train <- dtorders_final[inTrain, ]
df_test <- dtorders_final[-inTrain, ]

model_class_down_gbm <- train(form = review_score_bucket ~ .,
                             data = df_train,
                             method = "gbm",
                             tuneLength = 2,
                             trControl = trainControl(verboseIter = TRUE, 
                                                      method = "boot", 
                                                      number = 5, 
                                                      sampling = "down",
                                                      summaryFunction = twoClassSummary,#prSummarytwoClassSummary
                                                      classProbs = TRUE
                             ),
                             #tuneGrid = expand.grid(mtry = c(1:5)),
                             metric = "ROC"#AUC
)

model_class_down_gbm

pred  <- predict(model_class_down_gbm,df_test,type="prob")
#Show 10 probabilities
pred[1:10,]

result.roc <- roc(df_test$review_score_bucket, pred$Poor) # Draw ROC curve.
plot.roc(result.roc,print.thres = "best",print.auc = T,auc.polygon = T)





p_class_prob     <- as.factor(ifelse(pred$Poor>0.582,"Poor","Good"))
p_class_prob     <- relevel(p_class_prob, ref = "Poor")
confusionMatrix(data = p_class_prob, ref = df_test$review_score_bucket)

plot(1-result.roc$specificities,result.roc$sensitivities)

varImp(model_class_down_gbm)


