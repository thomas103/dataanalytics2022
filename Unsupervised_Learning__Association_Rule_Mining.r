library(data.table)
library(arules)
library(arulesViz)
library(dplyr)
library(timeDate)

#Data sourcing

dtorders        <- fread("/srv/data/retailusecase/Orders.csv")
dtorders_items  <- fread("/srv/data/retailusecase/Orders_Items.csv")
dtcust          <- fread("/srv/data/retailusecase/masterdata_customer.csv")
load("/srv/data/retailusecase/Orders_Reviews.rda",verbose = T)
dtorders_payment<- fread("/srv/data/retailusecase/Orders_Payment.csv")



#Categorize Review Scoring:
dtreviews           <- dtreviews%>%select(order_id,review_score)
dtreviews[dtreviews$review_score <3
          ,"ReviewBucket"]       <- "Poor"
dtreviews[dtreviews$review_score >=3 
          & dtreviews$review_score<=4
          ,"ReviewBucket"]       <- "Ok"
dtreviews[dtreviews$review_score >4
          ,"ReviewBucket"]       <- "Good"


#Feature Engineering

dtorders$IsLate         <- ifelse(dtorders$order_delivered_customer_date > dtorders$order_estimated_delivery_date,1,0)
dtorders$DeliveryDelay  <- as.integer(difftime(as.timeDate(dtorders$order_delivered_customer_date)
                                               ,as.timeDate(dtorders$order_estimated_delivery_date),units="days"))

dtorders                <- dtorders[is.na(dtorders$DeliveryDelay)==F,]
dtorders[dtorders$DeliveryDelay<=0 
         & dtorders$DeliveryDelay>=-16,"DelayCategory"] <- "Delivery:Well on Target"
dtorders[dtorders$DeliveryDelay>0 
         & dtorders$DeliveryDelay<=14,"DelayCategory"] <- "Delivery:Late"
dtorders[dtorders$DeliveryDelay>14 
          ,"DelayCategory"] <- "Delivery:Too Late"
dtorders[dtorders$DeliveryDelay< (-16) 
         ,"DelayCategory"] <- "Delivery:Exceeding Plan"


#Average & Total Order Value:
dtorders_aov    <- dtorders_items[,list(price=sum(price)
                                        ,orders=length(order_item_id)
                                        ,freight_value=sum(freight_value)
                                        ),by=c("order_id")]  

dtorders_aov$AOV              <- dtorders_aov$price/dtorders_aov$orders
dtorders_aov$TotalOrderValue  <- dtorders_aov$price
dtorders_aov$FreighRatio      <- dtorders_aov$freight_value/(dtorders_aov$price+dtorders_aov$freight_value)

dtorders_aov[dtorders_aov$FreighRatio<=quantile(dtorders_aov$FreighRatio)[3] 
         ,"FreightRatioCategory"] <- "FR:Below Average"
dtorders_aov[dtorders_aov$FreighRatio>quantile(dtorders_aov$FreighRatio)[3] 
         & dtorders_aov$FreighRatio<=quantile(dtorders_aov$FreighRatio)[4]
         ,"FreightRatioCategory"] <- "FR:Above Average Moderate"
dtorders_aov[dtorders_aov$FreighRatio>quantile(dtorders_aov$FreighRatio)[4] 
         ,"FreightRatioCategory"] <- "FR:High Above Average"


#Sellers Involved:
dtorders_sellers   <- unique(dtorders_items%>%select(order_id,seller_id))

#Customer Origin:
dtorders  <- merge(dtorders,dtcust,by=c("customer_id"))


#Bring all together and convert to transaction dataset:
dtorders_final  <- merge(dtorders,dtorders_aov,by=c("order_id"))
dtorders_final  <- merge(dtorders_final,dtorders_sellers,by=c("order_id"))

dtorders_final  <- dtorders_final%>%select(DelayCategory
                                           ,FreightRatioCategory
                                           ,seller_id
                                           ,customer_state
                                           ,order_id)

dtorders_final           <- merge(dtorders_final,dtreviews,by=c("order_id"))
dtorders_final           <- dtorders_final%>%select(-order_id,-review_score)
dforder_final            <- as.data.frame(dtorders_final)
for(i in 1:ncol(dforder_final)) dforder_final[[i]] <- as.factor(dforder_final[[i]])

transds  <- as(dforder_final, "transactions")



rules_poor <- apriori(transds, parameter = list(support = 0.001, confidence = 0.5)
                      ,appearance = list(rhs="ReviewBucket=Poor"))

dfrules_poor<- inspect(rules_poor)
dfrules_poor<- DATAFRAME(rules_poor)

dfrules_poor[order(-dfrules_poor$count),]

rules_good <- apriori(transds, parameter = list(support = 0.01, confidence = 0.65)
                      ,appearance = list(rhs="ReviewBucket=Good"))

dfrules_good<- inspect(rules_good)
dfrules_good<- DATAFRAME(rules_poor)


?apriori


ftb_repurchase_engineering  <- function(dtorders
                                        ,dtorders_aov
                                        ,dtreviews
                                        ,period=365){
  
  
  #1.) Set Churn-Label 
  #Calculate First and Last Time Purchase Date:
  dtlabel   <- dtorders[,list(FirstPurchaseDate=min(order_purchase_timestamp)
                            ,LastPurchaseDate=max(order_purchase_timestamp)
                            )
                      ,by=c("customer_unique_id")
                      ]
  
  dtlabel$TimeDelta <- as.double(difftime(as.timeDate(dtlabel$LastPurchaseDate)
                                        ,as.timeDate(dtlabel$FirstPurchaseDate),units = "days"))
  
  
  dtlabel$IsChurned <- ifelse(dtlabel$TimeDelta <= period
                            & dtlabel$TimeDelta >0
                            ,"No","Yes")
  
  #2.) Feature Engineering: FTP_NegativeReview, FTP_DelayCategory, FTP_FreightRatio:
  
  #2.1) First-Time-Purchase Negative Review
  dtftporders <- merge(dtorders,dtlabel,by.x=c("order_purchase_timestamp","customer_unique_id")
                                       ,by.y=c("FirstPurchaseDate","customer_unique_id")
                       )
  dtftporders <- merge(dtftporders,dtreviews,by=c("order_id"))
  dtftporders$FTP_ReviewBucket <- dtftporders$ReviewBucket
  
  #2.2)FTP_DelayCategory
  dtftporders$FTP_DelayCategory  <- dtftporders$DelayCategory
  
  #2.3)FTP_FreightRatio:
  dtftporders                           <- merge(dtftporders,dtorders_aov,by=c("order_id"))
  dtftporders$FTP_FreightRatioCategory  <- dtftporders$FreightRatioCategory
  
  #2.4) FTP Payment Type:
  dtorders_payment$Installment <- ifelse(dtorders_payment$payment_installments>1,"Yes","No")
  dtorders_payment  <- unique(dtorders_payment%>%select(order_id,payment_type,Installment))
  dtorders_payment  <- dtorders_payment[!duplicated(dtorders_payment$order_id),]
  
  return(dtftporders)

}





dtftporders <-  ftb_repurchase_engineering(dtorders
                                          ,dtorders_aov
                                          ,dtreviews
                                          ,period=365)




dtftporders_final           <- dtftporders%>%select(FTP_DelayCategory,FTP_FreightRatioCategory,FTP_ReviewBucket,Installment,payment_type,IsChurned,customer_state)
dfftporders_final            <- as.data.frame(dtftporders_final)
for(i in 1:ncol(dfftporders_final)) dfftporders_final[[i]] <- as.factor(dfftporders_final[[i]])

transds  <- as(dfftporders_final, "transactions")

rules_churnyes <- apriori(transds, parameter = list(support = 0.001, confidence = 0.999)
                      ,appearance = list(rhs="IsChurned=Yes"))

dfrules_churnyes<- inspect(rules_churnyes)
dfrules_churnyes<- DATAFRAME(rules_churnyes)


dfrules_churnyes[order(-dfrules_churnyes$lift),]


