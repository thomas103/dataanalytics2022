library(data.table)
library(lubridate)
library(dplyr)
library(leaflet)
library(arules)
library(arulesViz)
library(timeDate)
library(caret)
library(gbm)
library(corrplot)
library(pROC)
library(rgeos)
library(sf)
library(htmltools)

load("/srv/data/retailusecase/Orders_Reviews.rda",verbose = T)
load("/srv/data/retailusecase/l_shapefile_brazil.rda",verbose = T)
dtorders                <- fread("/srv/data/retailusecase/Orders.csv",encoding = "Latin-1")
dtorders_items          <- fread("/srv/data/retailusecase/Orders_Items.csv",encoding = "Latin-1")
dtproductfeatures       <- fread("/srv/data/retailusecase/Masterdata_product.csv",encoding = "Latin-1")
dtcust                  <- fread("/srv/data/retailusecase/masterdata_customer.csv",encoding = "Latin-1")


#Source: https://gadm.org/download_world.html --> shapefiles for all countries 
lnd_bra_level1  <- l_shapefile_brazil[["Level1"]]
lnd_bra_level2  <-  l_shapefile_brazil[["Level2"]] 


str(dtorders)

#1.)Data Preparation:

#1.1) Fulfilment Performance:
dtorders$DeliveryDelay_days   <- as.integer(difftime(as.timeDate(dtorders$order_delivered_customer_date)
                                                     ,as.timeDate(dtorders$order_estimated_delivery_date),units="days"))

dtorders                      <- dtorders[is.na(dtorders$DeliveryDelay)==F,]
dtorders$IsLate               <- ifelse(dtorders$DeliveryDelay_days<0,0,1)

dtorders$EstimatedDeliveryTime_days  <- as.integer(difftime(as.timeDate(dtorders$order_estimated_delivery_date)
                                                            ,as.timeDate(dtorders$order_purchase_timestamp),units="secs"))/(3600*24)

#1.2) Order Characteristics (Total Order Value, Freight Ratio)
dtorders_aov    <- dtorders_items[,list(price=sum(price)
                                        ,orders=length(order_item_id)
                                        ,freight_value=sum(freight_value)
),by=c("order_id")]  

dtorders_aov$TotalOrderValue  <- dtorders_aov$price
dtorders                      <- merge(dtorders,dtorders_aov,by=c("order_id"))


#1.3) Customer Characteristics:
dtorders                 <- merge(dtorders,dtcust,by=c("customer_id"))

#1.4) Review Merging:
dtgeoreview           <- merge(dtorders,dtreviews,by=c("order_id"))

#1.5) Final Preparation for output:
dtgeoagg  <- dtgeoreview[,list(ReviewScore=mean(review_score)
                               ,TotalOrderValue=sum(price)
                               ,freight_value=sum(freight_value)
                               ,IsLate=sum(IsLate)
                               ,orders=length(order_id)
                               ,DeliveryDelay_days=mean(DeliveryDelay_days)
                               ,EstimatedDeliveryTime_days=mean(EstimatedDeliveryTime_days)
                               )
                         ,by=c("Region_Level1","Region_Level1_ID")
                         ]
dtgeoagg  <- dtgeoagg[is.na(dtgeoagg$Region_Level1_ID)==F,]

shape_color_opacity_setting <- function(dtgeoagg){

  #1.) Reviews
  
    dtgeoagg$BM                 <- dtgeoagg$ReviewScore/mean(dtgeoreview$review_score)-1
    dtgeoagg$color_reviews      <- ifelse(dtgeoagg$BM <0,"#f92d00","#179334")
    dtgeoagg$Opacity_reviews    <- abs(dtgeoagg$BM)
      
    
  #2.) Late-Share
    dtgeoagg$IsLateShare <- dtgeoagg$IsLate/dtgeoagg$orders
    islateshare_total    <- sum(dtgeoagg$IsLate)/sum(dtgeoagg$orders)
    
    dtgeoagg$BM                   <- dtgeoagg$IsLateShare/islateshare_total-1
    dtgeoagg$color_lateshare      <- ifelse(dtgeoagg$BM >0,"#f92d00","#179334")
    dtgeoagg$Opacity_lateshare    <- abs(dtgeoagg$BM)
    
  #3.) Freight Ratio
    dtgeoagg$FreighRatio <- dtgeoagg$freight_value/(dtgeoagg$TotalOrderValue+dtgeoagg$freight_value)
    isfr_total           <- sum(dtgeoagg$freight_value)/(sum(dtgeoagg$TotalOrderValue)+sum(dtgeoagg$freight_value))
    
    dtgeoagg$BM             <- dtgeoagg$FreighRatio/isfr_total-1
    dtgeoagg$color_fr       <- ifelse(dtgeoagg$BM >0,"#f92d00","#179334")
    dtgeoagg$Opacity_fr     <- abs(dtgeoagg$BM)
    
  #4.) Total Order Value:
    dtgeoagg$totalOrderValueShare <- dtgeoagg$TotalOrderValue/sum(dtgeoagg$TotalOrderValue)
    dtgeoagg$color_ovs            <- "#0057f9"
    dtgeoagg$Opacity_ovs          <- abs(dtgeoagg$totalOrderValueShare)
    

  popup <- paste0("<b>",dtgeoagg$Region_Level1,"</b><br/>",
                   "Revenueshare: ",100*round(dtgeoagg$totalOrderValueShare,4),"%","<br/>",
                   "Late-Share: ",100*round(dtgeoagg$IsLateShare,4),"%","<br/>",
                   "Freight-Ratio: ",100*round(dtgeoagg$FreighRatio,4),"%","<br/>",
                  "Avg. Review: ",round(dtgeoagg$ReviewScore,2)
  )   
  dtgeoagg$popup      <- popup      
  dtgeoagg            <- dtgeoagg[order(dtgeoagg$Region_Level1_ID),]
  
  return(dtgeoagg)
}


dtgeoagg <- shape_color_opacity_setting(dtgeoagg )

map <- leaflet(dtorders) %>% addTiles() %>%
          addPolygons(data = lnd_bra_level1,color = "#bfb3b3", weight = 1, smoothFactor = 0.5,
              opacity = 1.0, fillOpacity = dtgeoagg$Opacity_reviews*10,
              fillColor = dtgeoagg$color_reviews,
              highlightOptions = highlightOptions(color = "white", weight = 4,
                                                  bringToFront = F)
              ,popup = dtgeoagg$popup,group="Reviews")%>%
          addPolygons(data = lnd_bra_level1,color = "#bfb3b3", weight = 1, smoothFactor = 0.5,
              opacity = 1.0, fillOpacity = dtgeoagg$Opacity_lateshare,
              fillColor = dtgeoagg$color_lateshare,
              highlightOptions = highlightOptions(color = "white", weight = 4,
                                                  bringToFront = F)
              ,popup = dtgeoagg$popup,group="Delivery Quality")%>%
          addPolygons(data = lnd_bra_level1,color = "#bfb3b3", weight = 1, smoothFactor = 0.5,
              opacity = 1.0, fillOpacity = dtgeoagg$Opacity_fr*10,
              fillColor = dtgeoagg$color_fr,
              highlightOptions = highlightOptions(color = "white", weight = 4,
                                                  bringToFront = F)
              ,popup = dtgeoagg$popup,group="Freight Ratio")%>%
         addPolygons(data = lnd_bra_level1,color = "#bfb3b3", weight = 1, smoothFactor = 0.5,
              opacity = 1.0, fillOpacity = dtgeoagg$Opacity_ovs*4,
              fillColor = dtgeoagg$color_ovs,
              highlightOptions = highlightOptions(color = "white", weight = 4,
                                                  bringToFront = F)
              ,popup = dtgeoagg$popup,group="Revenue-Share")%>%
          addLayersControl(
            overlayGroups = c("Reviews","Delivery Quality","Freight Ratio","Revenue-Share"),
            options = layersControlOptions(collapsed = T)
          )
  


map


