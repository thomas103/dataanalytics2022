library(data.table)
library(lubridate)
library(dplyr)
library(plotly)
library(car)
library(openxlsx)

#1.) Data Loading
dtorders              <- fread("/srv/data/retailusecase/Orders.csv",encoding = "Latin-1")
dtorders_items        <- fread("/srv/data/retailusecase/Orders_Items.csv",encoding = "Latin-1")
dtorders_price        <- dtorders_items[,list(price=sum(price)),by=c("order_id","seller_id")]
dtorders              <- merge(dtorders,dtorders_price,by=c("order_id"))
dtorders$PurchaseDate <- as.character(as.Date(dtorders$order_purchase_timestamp))

dtorders              <- dtorders[,list(Purchases=length(order_id)
                                        ,Price=sum(price)
),by=c("seller_id","PurchaseDate")]


glimpse(dtorders_items)

str(dtorders)

#RFM Function
rfm_function <- function(dt=dtorders
                         ,refdate=max(dtorders$PurchaseDate)
                         ,k=3
                         ,column="seller_id"
                         ){
  
  #1.) Recency:
  dt$Recency <- as.integer(difftime(as.Date(refdate) ,as.Date(dt$PurchaseDate) , units = c("days")))
  
  #2.) Frequency & Monetary:
  dt           <- dt[,list(Frequency=sum(Purchases)
                         ,Monetary=sum(Price)
                         ,Recency=min(Recency)
                         ),by=c(column)]
  
  dt$Monetary  <- dt$Monetary/dt$Frequency

  
  
  #Quantile Calculation:
  #v_rqtl <- quantile(dt$Recency,probs = seq(0.2,1,by=0.2))
  #v_fqtl <- quantile(dt$Frequency,probs = seq(0.2,1,by=0.2))
  #v_mqtl <- quantile(dt$Monetary,probs = seq(0.2,1,by=0.2))
  # dt$R_score  <- cut(dt$Recency,c(0,v_rqtl),labels=5:1)
  # dt$F_score  <- cut(dt$Frequency,c(0,v_fqtl),labels=1:5)
  # dt$M_score  <- cut(dt$Monetary,c(0,v_mqtl),labels=1:5)
  
  dt$R_score  <- 1- (dt$Recency-min(dt$Recency))/(max(dt$Recency)-min(dt$Recency))
  dt$F_score  <- (dt$Frequency-min(dt$Frequency))/(max(dt$Frequency)-min(dt$Frequency))
  dt$M_score  <- (dt$Monetary-min(dt$Monetary))/(max(dt$Monetary)-min(dt$Monetary))
  
  #Apply K-means:
  dttmp      <- dt%>%select(R_score,F_score,M_score)
  cluster    <- kmeans(dttmp,k)
  dt$Cluster <- cluster$cluster
  
  return(dt)
  
}

set.seed(1897)
dt           <- rfm_function(k=5)
dtclusteragg <- dt[,list(Recency=mean(Recency)
                        ,Frequency=mean(Frequency)
                        ,Monetary=mean(Monetary)
                        ,Count=length(seller_id)
                        ,FrequencyTotal=sum(Frequency)
),by=c("Cluster")]
dtclusteragg$FrequencyShare <- dtclusteragg$FrequencyTotal/sum(dtclusteragg$FrequencyTotal)
dtclusteragg[order(dtclusteragg$Recency),]

str(dt)

library(neighbr)
dt$Cluster <- factor(dt$Cluster)
dt$Id <- 1:nrow(dt)
dt$seller_id <- NULL
dt$Frequency <- NULL
dt$Monetary <- NULL
dt$Recency <- NULL

train_set <- dt[1:3090,]
test_set <- dt[3091:3095,1:3]
fit <- knn(train_set=train_set,test_set=test_set,
           k=5,
           categorical_target="Cluster",
          # continuous_target= "Petal.Width",
           comparison_measure="euclidean",
           return_ranked_neighbors=5,
           id="Id")


fit$test_set_scores

dt[3091:3095,]

#Plots with Plotly:

#i.) Scatterplot (Recency vs Frequency)
dt$Cluster  <- as.factor(dt$Cluster)
p           <- plot_ly(data = dt, x = ~Recency, y = ~Frequency, color = ~Cluster,type="scatter",mode="markers")
p



#ii.) Boxplot per Cluster
df      <- as.data.frame(dt)
p_rec   <- plot_ly(dt, y = ~Recency, color = ~Cluster, type = "box")%>%layout(title = "Recency")
p_freq  <- plot_ly(dt, y = ~Frequency, color = ~Cluster, type = "box")%>%layout(title = "Frequency")

p_rec
p_freq

#iii.) Violin Plot

p_rec <- dt %>%
  plot_ly(
    y = ~Recency,
    split = ~Cluster,
    type = 'violin',
    box = list(
      visible = T
    ),
    meanline = list(
      visible = T
    )
  ) %>% 
  layout(
    title="Recency",  
    yaxis = list(
      title = "Recency",
      zeroline = F
    )
  )

p_freq <- dt %>%
  plot_ly(
    y = ~Frequency,
    split = ~Cluster,
    type = 'violin',
    box = list(
      visible = T
    ),
    meanline = list(
      visible = T
    )
  ) %>% 
  layout(
    title="Frequency",  
    yaxis = list(
      title = "Frequency",
      zeroline = F
    )
  )

p_rec
p_freq

write.xlsx(dt,"rfm_seller.xlsx")

library(neighbr)
